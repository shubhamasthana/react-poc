import React from "react";
import styles from "./formWrapper.module.scss";

import InfraForm from "../../components/infrastructre-form/infra.form.component";

type myState = {
  selectedInfra: boolean;
  selectedvCloud: boolean;
};

class FormWrapper extends React.Component<{}, myState> {
  state = {
    selectedInfra: false,
    selectedvCloud: false
  };
  selectInfraHandler = () => {
    this.setState(prevState => {
      return {
        selectedInfra: !prevState.selectedInfra,
        selectedvCloud: false
      };
    });
  };

  selectvCloudHandler = () => {
    this.setState(prevState => {
      return {
        selectedInfra: false,
        selectedvCloud: !prevState.selectedvCloud
      };
    });
  };
  render() {
    const element = (
      <div className={styles.wrapper_container}>
        <div className={styles.wrapper_content}>
          <h2 className={styles.form_header}>
            VMware TCO Comparison Calculator
          </h2>
          <p className={styles.form_subheader}>
            See how Total Cost of Ownership is lower with VMware server
            virtualization and private cloud solutions compared to Microsoft
          </p>
        </div>
        <div className={styles.form_question}>
          <h3 className={styles.question_header}>1. Are You...</h3>
          <div className={styles.selection_tile}>
            <div
              className={`${styles.selection_tile_infra} ${
                this.state.selectedInfra ? styles.selected_infra : null
              } mt-2`}
              onClick={this.selectInfraHandler}
            >
              <span>Creating a new infrastructure?</span>
              <div className={styles.divider}>Customize your deployment</div>
            </div>
            <div
              className={`${styles.selection_tile_vcloud} ${
                this.state.selectedvCloud ? styles.selected_vcloud : null
              } mt-2`}
              onClick={this.selectvCloudHandler}
            >
              <span>Upgrading to vCloud Suite?</span>
              <div className={styles.divider}> Move up to private cloud</div>
            </div>
          </div>
          {this.state.selectedInfra ? <InfraForm></InfraForm> : null}
        </div>
      </div>
    );
    return element;
  }
}

export default FormWrapper;
