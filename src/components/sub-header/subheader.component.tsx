import React from 'react';
import styles from './subheader.module.scss';
import FormWrapper from '../form-wrapper/formwrapper.component';

const subHeaderList = ['Product', 'Cloud Services', 'Support', 'Downloads', 'Consulting', 'Partners', 'Company']

const subHeader: React.FC = ()=> {
    return (
             <div className ={`container mr-5 pr-4`}>
                 <ul className= {`${styles.subheader_list} justify-content-end`}>
                     {subHeaderList.map((item)=> {
                         return <li className= {`${styles.subheader_list_items} ml-5 mt-5`}>{item}</li>
                     })}
                 </ul>
                 <FormWrapper></FormWrapper>
             </div>)
}

export default subHeader;