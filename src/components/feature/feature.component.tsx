import React from "react";
import styles from "./featureComponent.module.scss";

const featureComponent: React.FC = () => {
  return (
    <div className={`${styles.wrapper} container`}>
      <h3>Choosen Feature</h3>
    </div>
  );
};

export default featureComponent;
