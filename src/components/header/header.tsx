import React from "react";
import styles from "./header.module.scss";
import SubHeader from "../sub-header/subheader.component";
const headerIcon = [
  "My Vmware",
  "Partner Central",
  "Training",
  "Community",
  "Store"
];
const header: React.FC = () => {
  return (
    <div className={styles.wrapper}>
      <nav
        className={`navbar navbar-expand-md navbar-dark bg-dark ${styles.navbar}`}
      >
        <span className="navbar-brand">India</span>
        <button className="navbar-toggler" type="button">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse">
          <div className="container">
            <ul className="navbar-nav justify-content-end">
              {headerIcon.map(icon => {
                return (
                  <li
                    className={`nav-item ${styles.nav_item} ${styles.icon_border}`}
                  >
                    {icon}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </nav>
      <SubHeader></SubHeader>
    </div>
  );
};
export default header;
