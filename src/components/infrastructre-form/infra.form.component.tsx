import React from "react";
import infraForm from "./infra-form.model";
import styles from "./infraform.module.scss";
import Wrapper from "../../hoc/wrapper.component";
import FeatureComponent from "../feature/feature.component";
class InfraForm extends React.Component<{}, infraForm> {
  options = ["select", "USD", "AUD", "AUR", "EUR", "GBP"];
  productOptions = [
    "vSphere Essentials",
    "vSphere Essential Plus",
    "vSphere Standard",
    "vSphere Enterprise plus",
    "vSphere Platinum",
    "vCloud Suite"
  ];
  state = {
    currency: "",
    virtualMachine: 0,
    product: 0,
    hostType: "",
    storageType: "",
    infraCost: "",
    dataCost: "",
    vmDensity: 0,
    additionalVm: 0,
    timeFrame: 0
  };
  render() {
    const element = (
      <div>
        <form>
          <div>
            <h3 className={styles.question_header}>2. Currency</h3>
            <select className={styles.currency_dropdown}>
              {this.options.map(option => {
                return <option>{option}</option>;
              })}
            </select>
          </div>
          <div>
            <h3 className={styles.question_header}>
              3. How many virtual machine do you want to deploy?
            </h3>
            <input
              className={styles.vm_input}
              placeholder="Enter the number of VMs desired"
            ></input>
          </div>
          <div>
            <h3 className={styles.question_header}>
              4. Which VMware product edition would you like to compare with
              Microsoft's solution?
            </h3>
            <div className={`${styles.product_wrapper}`}>
              {this.productOptions.map(option => {
                return (
                  <Wrapper>
                    <div className={`${styles.product_options} pb-3`}>
                      <label className="">{option}</label>
                      <input className="" type="radio" />
                    </div>
                  </Wrapper>
                );
              })}
            </div>
            <div>
              <h3 className={styles.question_header}>
                5. Additional VMware VMs per CPU
              </h3>
            </div>
          </div>
        </form>
        <FeatureComponent></FeatureComponent>
      </div>
    );
    return element;
  }
}
export default InfraForm;
