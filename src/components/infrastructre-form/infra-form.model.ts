class InfraForm {
    currency: string;
    virtualMachine: number;
    product: number;
    hostType: string;
    storageType: string;
    infraCost: string;
    dataCost: string;
    vmDensity: number;
    additionalVm: number;
    timeFrame: number;
}
export default InfraForm;